<?php

require_once dirname(__DIR__).DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'autoload.php';

\App\Dumper::showErrors();
\App\Dumper::dump($_SERVER);

$altoApp = new \App\Application();
$altoApp->launch();


<?php
namespace App;

class Dumper
{

    public static function dump($var)
    {
        echo "<pre>";
        print_r($var);
        echo "</pre>";
        return;
    }

    public static function showErrors()
    {
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        return;
    }
}
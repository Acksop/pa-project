<?php
namespace App;

class Application
{

    private $router;
    private $match;

    public function __construct()
    {

        $this->router = new \AltoRouter();
    
        $this->router->setBasePath(BASE_PATH);

        $this->router = \App\Routes::mapRouter($this->router);

        $this->match = $this->router->match();
    }

    public function launch()
    {
        // call closure or throw 404 status
        if( is_array($this->match) && is_callable( $this->match['target'] ) ) {
            call_user_func_array( $this->match['target'], $this->match['params'] ); 
        } else {
            // no route was matched
            header( $_SERVER["SERVER_PROTOCOL"] . ' 404 Not Found');
        }
    }

}
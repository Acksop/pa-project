<?php
namespace App;

class TwigBaseController
{

    private $twig;

    public function __construct()
    {
        $loader = new \Twig\Loader\FilesystemLoader(dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR."template");
        $this->twig = new \Twig\Environment($loader, [
            'cache' => dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR."cache",
        ]);
    }
}
<?php
namespace App;


class Routes
{

    public static function mapRouter($router)
    {

        // map homepage
        $router->map( 'GET', '/', function() {
            $page = new \App\Controller\HomeController();
            $page->home();
        });

        // map user details page
        $router->map( 'GET', '/user/[i:id]/', function( $id ) {
            $page = new \App\Controller\HomeController();
            $page->user($id);
        });

        // map other routes
        // $router->map( 'GET|POST', '/myroute/[i:myvar]/', function( $myvar ) {
        //     $page = new \App\Controller\MyController();
        //     $page->myAction($myvar);
        // });

        return $router;
    }
}
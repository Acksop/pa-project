<?php
namespace App\Controller;

class HomeController extends \App\TwigBaseController
{

    public function home()
    {
        echo $this->twig->render('index.html.twig', ['the' => 'variables', 'go' => 'here']);
    }

    public function user($id)
    {
        echo $this->twig->render('user.html.twig', ['id' => $id]);
    }

}

